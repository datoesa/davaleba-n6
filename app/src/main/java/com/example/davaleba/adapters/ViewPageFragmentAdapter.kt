package com.example.davaleba.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.davaleba.fragments.FragmentOne
import com.example.davaleba.fragments.FragmentSecond
import com.example.davaleba.fragments.FragmentThird

class ViewPageFragmentAdapter(activity: FragmentActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount() = 3

    override fun createFragment(position: Int): Fragment {
        return when (position){
            0 -> FragmentOne()
            1 -> FragmentSecond()
            2 -> FragmentThird()
            else -> FragmentOne()
        }
    }
}