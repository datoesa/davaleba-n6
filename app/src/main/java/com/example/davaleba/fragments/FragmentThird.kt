package com.example.davaleba.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.davaleba.R

class FragmentThird : Fragment(R.layout.fragment_third) {
    private lateinit var addButton: Button
    private lateinit var noteEditText: EditText
    private lateinit var textView: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addButton = view.findViewById(R.id.buttonAdd)
        noteEditText = view.findViewById(R.id.editTextNote)
        textView = view.findViewById(R.id.textView)

        val sharedPreferences =
            requireActivity().getSharedPreferences("APP_PR", AppCompatActivity.MODE_PRIVATE)
        val notes = sharedPreferences.getString("NOTES", "")
        textView.text = notes

        addButton.setOnClickListener {
            val note = noteEditText.text.toString()
            val text = textView.text.toString()
            val result = note + "\n" + text
            textView.text = result

            sharedPreferences.edit()
                .putString("NOTES", result)
                .apply()

        }
    }
}